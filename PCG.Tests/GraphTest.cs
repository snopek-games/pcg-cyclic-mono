using NUnit.Framework;
using System.Collections.Generic;

namespace PCG.Tests
{
    public class GraphTest
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestStringConstraint()
        {
            var no_constraints = new StringConstraint();
            Assert.True(no_constraints.Matches("anything"));
            
            var star_constraint = new StringConstraint("*");
            Assert.True(star_constraint.Matches("anything"));

            var simple_constraint = new StringConstraint("the_value");
            Assert.True(simple_constraint.Matches("the_value"));
            Assert.False(simple_constraint.Matches("another_value"));

            var multi_constraint = new StringConstraint();
            multi_constraint.AddAllowedValue("allowed1");
            multi_constraint.AddAllowedValue("allowed2");
            Assert.True(multi_constraint.Matches("allowed1"));
            Assert.True(multi_constraint.Matches("allowed2"));
            Assert.False(multi_constraint.Matches("not_allowed"));

            var disallowed = new StringConstraint();
            disallowed.AddDisallowedValue("not_this_thing");
            Assert.True(disallowed.Matches("anything"));
            Assert.False(disallowed.Matches("not_this_thing"));
        }

        [Test]
        public void TestStringConstraintMatchesDictionary() 
        {
            var constraints = new Dictionary<string, StringConstraint>
            {
                ["first"] = new StringConstraint("first_value"),
                ["second"] = new StringConstraint("second_value")
            };

            var passing_dictionary = new Dictionary<string, string>();
            passing_dictionary["first"] = "first_value";
            passing_dictionary["second"] = "second_value";
            Assert.True(StringConstraint.MatchesDictionary(constraints, passing_dictionary));

            var failing_dictionary = new Dictionary<string, string>();
            failing_dictionary["first"] = "first_value";
            failing_dictionary["second"] = "not_the_right_value";
            Assert.False(StringConstraint.MatchesDictionary(constraints, failing_dictionary));

            var sparse_dictionary = new Dictionary<string, string>();
            sparse_dictionary["first"] = "first_value";
            Assert.False(StringConstraint.MatchesDictionary(constraints, sparse_dictionary));
        }

        [Test]
        public void TestRoomConstraint()
        {
            var empty_room = new Room();
            
            var treasure_room = new Room("treasure");
            treasure_room.Attributes["treasure_type"] = "weapon";

            var no_constraints = new RoomConstraint();
            Assert.True(no_constraints.Matches(treasure_room));

            var type_constraint = new RoomConstraint(new StringConstraint("treasure"));
            Assert.True(type_constraint.Matches(treasure_room));
            Assert.False(type_constraint.Matches(empty_room));

            var passing_attribute_contraint = new RoomConstraint(
                new StringConstraint("treasure"),
                new Dictionary<string, StringConstraint>{ {"treasure_type", new StringConstraint("weapon") } }
            );
            Assert.True(passing_attribute_contraint.Matches(treasure_room));

            var failing_attribute_constraint = new RoomConstraint(
                new StringConstraint("treasure"),
                new Dictionary<string, StringConstraint>{ {"treasure_type", new StringConstraint("potion") } }
            );
        }

        // @todo Test connection constraints

        [Test]
        public void TestValidateConstraints() {
            InvalidConstraintsException exception;

            var empty_constraints = new Constraints();
            exception = Assert.Throws<InvalidConstraintsException>(delegate { empty_constraints.Validate(); });
            Assert.AreEqual(exception.Message, "Must have at least one RoomConstraint");

            var simple_constraints = new Constraints();
            simple_constraints.AddRoomConstraint("room", new RoomConstraint());
            Assert.DoesNotThrow(delegate { simple_constraints.Validate(); });

            var connection_constraints = new Constraints();
            connection_constraints.AddRoomConstraint("room1", new RoomConstraint());
            connection_constraints.AddRoomConstraint("room2", new RoomConstraint());
            connection_constraints.AddConnectionConstraint("connection", new ConnectionConstraint("room1", "room2"));
            Assert.DoesNotThrow(delegate { simple_constraints.Validate(); });

            var invalid_connection_constraints = new Constraints();
            invalid_connection_constraints.AddRoomConstraint("room1", new RoomConstraint());
            invalid_connection_constraints.AddConnectionConstraint("connection", new ConnectionConstraint("room1", "room2"));
            exception = Assert.Throws<InvalidConstraintsException>(delegate { invalid_connection_constraints.Validate(); });
            Assert.AreEqual(exception.Message, "ConnectionConstraint 'connection' has target 'room1' without RoomConstraint");

            var unconnected_rooms_constraints = new Constraints();
            unconnected_rooms_constraints.AddRoomConstraint("room1", new RoomConstraint());
            unconnected_rooms_constraints.AddRoomConstraint("room2", new RoomConstraint());
            unconnected_rooms_constraints.AddRoomConstraint("room3", new RoomConstraint());
            unconnected_rooms_constraints.AddConnectionConstraint("connection", new ConnectionConstraint("room1", "room2"));
            exception = Assert.Throws<InvalidConstraintsException>(delegate { unconnected_rooms_constraints.Validate(); });
            Assert.AreEqual(exception.Message, "The following room constraints don't appear to be connected to the others: room3");
        }

        [Test]
        public void TestGraphFindMatch()
        {
            var graph = new Graph();

            var start = new PCG.Room("start");
            var end = new PCG.Room("end");
            var connection = new PCG.Connection(start, end);

            graph.AddVertex(start);
            graph.AddVertex(end);
            graph.AddEdge(connection);

            List<Match> matches = null;

            var start_constraints = new Constraints();
            start_constraints.AddRoomConstraint("start", new RoomConstraint(new StringConstraint("start")));
            matches = graph.FindMatches(start_constraints);
            Assert.AreEqual(1, matches.Count);
            Assert.AreEqual(start, matches[0].Rooms["start"]);

            var end_constraints = new Constraints();
            end_constraints.AddRoomConstraint("end", new RoomConstraint(new StringConstraint("end")));
            matches = graph.FindMatches(end_constraints);
            Assert.AreEqual(1, matches.Count);
            Assert.AreEqual(end, matches[0].Rooms["end"]);

            var any_constraints = new Constraints();
            any_constraints.AddRoomConstraint("room", new RoomConstraint());
            matches = graph.FindMatches(any_constraints);
            Assert.AreEqual(2, matches.Count);
            Assert.AreEqual(start, matches[0].Rooms["room"]);
            Assert.AreEqual(end, matches[1].Rooms["room"]);

            var start_to_end_constraints = new Constraints();
            start_to_end_constraints.AddRoomConstraint("start", new RoomConstraint(new StringConstraint("start")));
            start_to_end_constraints.AddRoomConstraint("end", new RoomConstraint(new StringConstraint("end")));
            start_to_end_constraints.AddConnectionConstraint("connection", new ConnectionConstraint("start", "end"));
            matches = graph.FindMatches(start_to_end_constraints);
            Assert.AreEqual(1, matches.Count);
            Assert.AreEqual(start, matches[0].Rooms["start"]);
            Assert.AreEqual(end, matches[0].Rooms["end"]);
            Assert.AreEqual(connection, matches[0].Connections["connection"]);
        }

        [Test]
        public void FindMatchBidirectional() {
            var graph = new Graph();

            var start = new PCG.Room("start");
            var end = new PCG.Room("end");
            var connection = new PCG.Connection(start, end);

            graph.AddVertex(start);
            graph.AddVertex(end);
            graph.AddEdge(connection);

            List<Match> matches = null;

            var constraints = new Constraints();
            constraints.AddRoomConstraint("start", new RoomConstraint(new StringConstraint("start")));
            constraints.AddRoomConstraint("end", new RoomConstraint(new StringConstraint("end")));
            var connectionConstraint = new ConnectionConstraint("end", "start");
            constraints.AddConnectionConstraint("connection", connectionConstraint);

            // We won't find a match when done not bidirectionally.
            matches = graph.FindMatches(constraints);
            Assert.AreEqual(0, matches.Count);

            // We'll get one match when the connection is found bidirectionally.
            connectionConstraint.Bidirectional = true;
            matches = graph.FindMatches(constraints);
            Assert.AreEqual(1, matches.Count);
            Assert.AreEqual(connection, matches[0].Connections["connection"]);
        }
    }
}