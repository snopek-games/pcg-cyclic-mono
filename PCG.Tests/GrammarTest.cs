using NUnit.Framework;
using System.Linq;

namespace PCG.Tests
{
    public class GrammarTest {
 
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestConnectStartToEndTransformation()
        {
            var graph = new Graph();
            var start = new Room("start");
            var end = new Room("end");
            var connection = new Connection(start, end);

            graph.AddVertex(start);
            graph.AddVertex(end);
            graph.AddEdge(connection);

            var tranformations = new PCG.Transformations.ConnectStartToEnd();
            var matches = tranformations.FindMatches(graph);
            tranformations.Execute(graph, matches[0]);

            var rooms = graph.Vertices.ToList();
            Assert.AreEqual(4, rooms.Count);
            Assert.AreEqual(start, rooms[0]);
            Assert.AreEqual(end, rooms[1]);
            Assert.AreEqual("empty", rooms[2].Type);
            Assert.AreEqual("empty", rooms[3].Type);

            var connections = graph.Edges.ToList();
            Assert.AreEqual(4, connections.Count);
            Assert.AreEqual(start, connections[0].Source);
            Assert.AreEqual(rooms[2], connections[0].Target);
            Assert.AreEqual(start, connections[1].Source);
            Assert.AreEqual(rooms[3], connections[1].Target);
            Assert.AreEqual(rooms[2], connections[2].Source);
            Assert.AreEqual(end, connections[2].Target);
            Assert.AreEqual(rooms[3], connections[3].Source);
            Assert.AreEqual(end, connections[3].Target);
        }

        [Test]
        public void TestLockEndRoomTransformation()
        {
            var graph = new Graph();
            var room = new Room("empty");
            var end = new Room("end");
            var connection = new Connection(room, end);

            graph.AddVertex(room);
            graph.AddVertex(end);
            graph.AddEdge(connection);

            var tranformations = new PCG.Transformations.LockEndRoom();
            var matches = tranformations.FindMatches(graph);
            tranformations.Execute(graph, matches[0]);

            var rooms = graph.Vertices.ToList();
            Assert.AreEqual(3, rooms.Count);
            Assert.AreEqual(room, rooms[0]);
            Assert.AreEqual("empty", rooms[1].Type);
            Assert.AreEqual("end", rooms[2].Type);

            var connections = graph.Edges.ToList();
            Assert.AreEqual(2, connections.Count);
            Assert.AreEqual("open", connections[0].Type);
            Assert.AreEqual("locked", connections[1].Type);
        }
    }
}