using System;
using System.Collections.Generic;
using NUnit.Framework;
using QuikGraph;

namespace PCG.Tests
{
    public class PlotterTest
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestStartEndOnly()
        {
            var graph = new PCG.Graph();
            
            var start = new PCG.Room("start");
            var end = new PCG.Room("end");
            var connection = new PCG.Connection(start, end);

            graph.AddVertex(start);
            graph.AddVertex(end);
            graph.AddEdge(connection);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(end, grid[0, 0]);
            Assert.AreEqual(start, grid[1, 0]);
        }

        [Test]
        public void TestStartMiddleEnd()
        {
            var graph = new PCG.Graph();
            
            var start = new PCG.Room("start");
            var middle = new PCG.Room("middle");
            var end = new PCG.Room("end");
            var connection1 = new PCG.Connection(start, middle);
            var connection2 = new PCG.Connection(middle, end);

            graph.AddVertex(start);
            graph.AddVertex(middle);
            graph.AddVertex(end);
            graph.AddEdge(connection1);
            graph.AddEdge(connection2);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(end, grid[0, 0]);
            Assert.AreEqual(middle, grid[1, 0]);
            Assert.AreEqual(start, grid[2, 0]);
        }

        [Test]
        public void TestMiddleBranch()
        {
            var graph = new PCG.Graph();
            
            var start = new PCG.Room("start");
            var middle = new PCG.Room("middle");
            var middle_branch = new PCG.Room("middle_branch");
            var end = new PCG.Room("end");
            var connection1 = new PCG.Connection(start, middle);
            var connection2 = new PCG.Connection(middle, end);
            var connection3 = new PCG.Connection(middle, middle_branch);

            graph.AddVertex(start);
            graph.AddVertex(middle);
            graph.AddVertex(middle_branch);
            graph.AddVertex(end);
            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(end, grid[0, 0]);
            Assert.AreEqual(middle, grid[1, 0]);
            Assert.AreEqual(start, grid[1, 1]);
            Assert.AreEqual(middle_branch, grid[2, 0]);
        }

        [Test]
        public void TestSimplestCycle()
        {
            var graph = new PCG.Graph();
            
            var start = new PCG.Room("start");
            var middle1 = new PCG.Room("middle1");
            var middle2 = new PCG.Room("middle2");
            var end = new PCG.Room("end");
            var connection1 = new PCG.Connection(start, middle1);
            var connection2 = new PCG.Connection(start, middle2);
            var connection3 = new PCG.Connection(middle1, end);
            var connection4 = new PCG.Connection(middle2, end);

            graph.AddVertex(start);
            graph.AddVertex(middle1);
            graph.AddVertex(middle2);
            graph.AddVertex(end);

            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);
            graph.AddEdge(connection4);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(end, grid[0, 0]);
            Assert.AreEqual(middle1, grid[1, 0]);
            Assert.AreEqual(middle2, grid[0, 1]);
            Assert.AreEqual(start, grid[1, 1]);
        }

        [Test]
        public void TestAddPathToCompleteOtherwiseImpossibleCycle1() {
            var graph = new PCG.Graph();
            
            var start = new PCG.Room("start");
            var alternate = new PCG.Room("alternate");
            var end = new PCG.Room("end");
            var connection1 = new PCG.Connection(start, end);
            var connection2 = new PCG.Connection(start, alternate);
            var connection3 = new PCG.Connection(alternate, end);

            graph.AddVertex(start);
            graph.AddVertex(alternate);
            graph.AddVertex(end);

            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(end, grid[0, 0]);
            Assert.AreEqual(alternate, grid[0, 1]);
            Assert.AreEqual(start, grid[1, 0]);           
            Assert.AreEqual("path", grid[1, 1].Type);
        }

        [Test]
        public void TestAddPathToCompleteOtherwiseImpossibleCycle2() {
            var graph = new Graph();

            var start = new PCG.Room("start");
            var end = new PCG.Room("end");
            var room1 = new Room("room1");
            var room2 = new Room("room2");
            var connection1 = new Connection(start, room1);
            var connection2 = new Connection(room1, room2);
            var connection3 = new Connection(room2, end);
            var connection4 = new Connection(room2, start);

            graph.AddVertex(start);
            graph.AddVertex(end);
            graph.AddVertex(room1);
            graph.AddVertex(room2);
            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);
            graph.AddEdge(connection4);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(end, grid[0, 0]);
            Assert.AreEqual(null, grid[0, 1]);
            Assert.AreEqual(room2, grid[1, 0]);
            Assert.AreEqual(room1, grid[1, 1]);
            Assert.AreEqual(start, grid[2, 0]);
            Assert.AreEqual("path", grid[2, 1].Type);
        }

        [Test]
        public void TestAddPathToCompleteOtherwiseImpossibleCycle3() {
            var graph = new Graph();

            var start = new PCG.Room("start");
            var end = new PCG.Room("end");
            var room1 = new Room("room1");
            var room2 = new Room("room2");
            var connection1 = new Connection(start, room1);
            var connection2 = new Connection(room1, room2);
            var connection3 = new Connection(room2, end);
            var connection4 = new Connection(end, room1);

            graph.AddVertex(start);
            graph.AddVertex(end);
            graph.AddVertex(room1);
            graph.AddVertex(room2);
            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);
            graph.AddEdge(connection4);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(room2, grid[0, 0]);
            Assert.AreEqual(end, grid[0, 1]);
            Assert.AreEqual(null, grid[0, 2]);
            Assert.AreEqual("path", grid[1, 0].Type);
            Assert.AreEqual(room1, grid[1, 1]);
            Assert.AreEqual(start, grid[1, 2]);
        }

        [Test]
        public void TestAddPathToCompleteOtherwiseImpossibleCycle4() {
            var graph = new Graph();

            // First loop.
            var start = new PCG.Room("start");
            var room1 = new Room("room1");
            var room2 = new Room("room2");
            var connection1 = new Connection(start, room1);
            var connection2 = new Connection(room1, room2);
            var connection3 = new Connection(room2, start);

            // Second loop.
            var room3 = new Room("room3");
            var room4 = new Room("room4");
            var connection4 = new Connection(room2, room3);
            var connection5 = new Connection(room3, room4);
            var connection6 = new Connection(room4, start);

            // Path to the end.
            var end = new Room("end");
            var connection7 = new Connection(room4, end);

            graph.AddVertex(start);
            graph.AddVertex(room1);
            graph.AddVertex(room2);
            graph.AddVertex(room3);
            graph.AddVertex(room4);
            graph.AddVertex(end);
            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);
            graph.AddEdge(connection4);
            graph.AddEdge(connection5);
            graph.AddEdge(connection6);
            graph.AddEdge(connection7);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.AreEqual(end, grid[0, 0]);
            Assert.AreEqual(null, grid[0, 1]);
            Assert.AreEqual(room4, grid[1, 0]);
            Assert.AreEqual(room3, grid[1, 1]);
            Assert.AreEqual(start, grid[2, 0]);
            Assert.AreEqual(room2, grid[2, 1]);
            Assert.AreEqual("path", grid[3, 0].Type);
            Assert.AreEqual(room1, grid[3, 1]);
        }

        [Test]
        public void TestAddPathToCompleteOtherwiseImpossibleCycle5() {
            var graph = new Graph();

            // First loop.
            var start = new PCG.Room("start");
            var room1 = new Room("room1");
            var room2 = new Room("room2");
            var connection1 = new Connection(start, room1);
            var connection2 = new Connection(start, room2);
            var connection3 = new Connection(room1, room2);

            // Second loop.
            var room3 = new Room("room3");
            var room4 = new Room("room4");
            var connection4 = new Connection(room2, room3);
            var connection5 = new Connection(room2, room4);
            var connection6 = new Connection(room3, room4);

            // Path to the end.
            var end = new Room("end");
            var connection7 = new Connection(room4, end);

            graph.AddVertex(start);
            graph.AddVertex(room1);
            graph.AddVertex(room2);
            graph.AddVertex(room3);
            graph.AddVertex(room4);
            graph.AddVertex(end);
            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);
            graph.AddEdge(connection4);
            graph.AddEdge(connection5);
            graph.AddEdge(connection6);
            graph.AddEdge(connection7);

            var plotter = new PCG.Plotter(graph);
            while (plotter.Plot()) { }
            var grid = plotter.GetGrid();

            Assert.Pass();
        }

        [Test]
        public void TestImpossibleCycle() {
            var graph = new PCG.Graph();
            
            var start = new PCG.Room("start");
            var alternate1 = new PCG.Room("alternate");
            var alternate2 = new PCG.Room("alternate");
            var end = new PCG.Room("end");

            graph.AddVertex(start);
            graph.AddVertex(alternate1);
            graph.AddVertex(alternate2);
            graph.AddVertex(end);

            graph.AddEdgeRange(new List<Connection> {
                new PCG.Connection(start, end),
                new PCG.Connection(start, alternate1),
                new PCG.Connection(start, alternate2),
                new PCG.Connection(start, end),
                new PCG.Connection(alternate1, end),
                new PCG.Connection(alternate1, alternate2),
                new PCG.Connection(alternate2, end),
            });

            var plotter = new PCG.Plotter(graph);

            Assert.Throws<PCG.UnableToPlotGraphException>(
                delegate { while(plotter.Plot()) { } });
        }
 
    }

}
