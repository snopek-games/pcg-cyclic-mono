using System;
using System.Collections.Generic;
using PCG;

namespace TestGrammar1 {
    class AddLockedValveLoop : ITransformationRule {
        public int GetWeight() { return 50; }

        public List<Match> FindMatches(Graph graph) {
            var constraints = new Constraints();
            constraints.AddRoomConstraint("room1", new RoomConstraint(){ MaxConnections = 3 });
            constraints.AddRoomConstraint("room2", new RoomConstraint(){ MaxConnections = 3 });
            var connectionConstraint = new ConnectionConstraint("room1", "room2", new StringConstraint("open"));
            constraints.AddConnectionConstraint("connection", connectionConstraint);
            return graph.FindMatches(constraints);
        }

        public void Execute(Graph graph, Match match) {
            graph.RemoveEdge(match.Connections["connection"]);
            var path1 = new Room();
            var path2 = new Room();
            var connection1 = new Connection(match.Rooms["room1"], path1);
            var connection2 = new Connection(path1, path2);
            connection2.Type = "locked";

            var floorSwitch1 = new GameObject("floor_switch");
            floorSwitch1.References["unlocks"] = connection2;
            path1.GameObjects.Add(floorSwitch1);

            var connection3 = new Connection(path2, match.Rooms["room2"]);
            var connection4 = new Connection(path2, match.Rooms["room1"]);
            connection4.Type = "locked";

            var floorSwitch2 = new GameObject("floor_switch");
            floorSwitch2.References["unlocks"] = connection4;
            path2.GameObjects.Add(floorSwitch2);

            graph.AddVertex(path1);
            graph.AddVertex(path2);
            graph.AddEdge(connection1);
            graph.AddEdge(connection2);
            graph.AddEdge(connection3);
            graph.AddEdge(connection4);

        }

    }
}
