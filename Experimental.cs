using Godot;
using System;
using QuikGraph;

class FooVertex {
    public string type;
    public int x;
    public int y;

    public FooVertex(int _x, int _y, string _type) {
        x = _x;
        y = _y;
        type = _type;
    }
};

class FooEdge : Edge<FooVertex> {
    public FooEdge(FooVertex a, FooVertex b) : base(a, b) {}
};

class FooGraph : AdjacencyGraph<FooVertex, FooEdge> {};

public class Experimental : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        var graph = new FooGraph();
        
        var vertex1 = new FooVertex(0, 0, "empty");
        var vertex2 = new FooVertex(0, 1, "empty");
        graph.AddVertex(vertex1);
        graph.AddVertex(vertex2);

        var edge1 = new FooEdge(vertex1, vertex2);
        graph.AddEdge(edge1);

        foreach (var edge in graph.Edges) {
            GD.Print(edge.Source.type + " => " + edge.Target.type);
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
