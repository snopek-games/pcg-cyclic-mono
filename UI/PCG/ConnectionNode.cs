using Godot;
using System;

public class ConnectionNode : Node2D
{
    [Export]
    public string Type {
        get { return GetNode<Label>("TypeLabel").Text; }
        set
        {
            var typeLabel = GetNode<Label>("TypeLabel");
            if (typeLabel.Text != value) {
                typeLabel.Text = value;
            }
        }
    }
    
}
