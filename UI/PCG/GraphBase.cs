using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using PCG;

public class GraphBase : Node2D
{
    protected Graph _graph;

    protected Dictionary<Room, RoomNode> _roomNodeMap;
    protected Dictionary<Connection, ConnectionNode> _connectionNodeMap;

    protected PackedScene _connectionNodePackedScene;
    protected PackedScene _roomNodePackedScene;
    protected Node2D _roomsContainer;
    protected Node2D _connectionsContainer;
    protected DetailsDisplay _attributesDisplay;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _roomNodePackedScene = GD.Load("res://UI/PCG/RoomNode.tscn") as PackedScene;
        _connectionNodePackedScene = GD.Load("res://UI/PCG/ConnectionNode.tscn") as PackedScene;

        _roomsContainer = GetNode<Node2D>("Rooms");
        _connectionsContainer = GetNode<Node2D>("Connections");
        _attributesDisplay = GetNode<DetailsDisplay>("DetailsDisplay");

        _roomNodeMap = new Dictionary<Room, RoomNode>();
        _connectionNodeMap = new Dictionary<Connection, ConnectionNode>();
    }

    public override void _Draw()
    {
        if (_graph == null) {
            return;
        }

        foreach (var connection in _graph.Edges) {
            var roomNode1 = _roomNodeMap[connection.Source];
            var roomNode2 = _roomNodeMap[connection.Target];
            if (roomNode1.Visible && roomNode2.Visible) {
                DrawLine(roomNode1.Position, roomNode2.Position, new Color(1.0f, 1.0f, 1.0f, 1.0f), 1.0f, true);
            }
        }

    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        RepositionRooms();
        RepositionConnections();
        Update();
    }

    virtual protected void RepositionRooms() {
    }

    virtual protected void RepositionConnections() {
        foreach (var connection in _graph.Edges) {
            var connectionNode = _connectionNodeMap[connection];
            if (!Godot.Object.IsInstanceValid(connectionNode)) {
                continue;
            }

            var roomNode1 = _roomNodeMap[connection.Source];
            var roomNode2 = _roomNodeMap[connection.Target];
            if (!roomNode1.Visible || !roomNode2.Visible) {
                connectionNode.Visible = false;
            }
            else {
                connectionNode.Visible = true;
            }
            connectionNode.Position = (roomNode1.Position + roomNode2.Position) / 2;
            if (roomNode1.Position.x <= roomNode2.Position.x) {
                connectionNode.Rotation = roomNode1.GetAngleTo(roomNode2.Position);
            }
            else {
                connectionNode.Rotation = roomNode2.GetAngleTo(roomNode1.Position);
            }

            // Unrelated hack: make sure displayed type matches graph node.
            connectionNode.Type = connection.Type;
        }
    }
    protected void ShowDetails(Vector2 position, Dictionary<string, string> attributes, List<GameObject> gameObjects = null) {
        if (attributes.Count > 0 || (gameObjects != null && gameObjects.Count > 0)) {
            _attributesDisplay.Show();
            _attributesDisplay.GlobalPosition = position;
            _attributesDisplay.ShowDetails(attributes, gameObjects);
        }
        else {
            _attributesDisplay.Hide();
        }
    }

    protected Node2D GetClosestNode(Godot.Collections.Array nodes, Vector2 position, float distance) {
        float minDistance = Mathf.Inf;
        Node2D closestNode = null;
        foreach (Node2D roomNode in nodes) {
            float distanceToNode = position.DistanceTo(roomNode.GlobalPosition);
            if (distanceToNode < minDistance) {
                minDistance = distanceToNode;
                closestNode = roomNode;
            }
        }
        if (minDistance < distance) {
            return closestNode;
        }
        return null;
    }

    protected void ClearGraph() {
        // Clear out all the room and connection nodes.
        foreach (Node2D child in _roomsContainer.GetChildren()) {
            _roomsContainer.RemoveChild(_roomsContainer);
            child.QueueFree();
        }
        foreach (Node2D child in _connectionsContainer.GetChildren()) {
            _connectionsContainer.RemoveChild(_connectionsContainer);
            child.QueueFree();
        }
        
        // Reset the node and connection map.
        _roomNodeMap = new Dictionary<Room, RoomNode>();
        _connectionNodeMap = new Dictionary<Connection, ConnectionNode>();

        if (_graph != null) {
            _graph.VertexAdded -= AddRoomNode;
            _graph.VertexRemoved -= RemoveRoomNode;
            _graph.EdgeAdded -= AddConnectionNode;
            _graph.EdgeRemoved -= RemoveConnectionNode;
            _graph = null;
        }
    }

    virtual public void SetGraph(Graph newGraph) {
        ClearGraph();

        _graph = newGraph;
        _graph.VertexAdded += AddRoomNode;
        _graph.VertexRemoved += RemoveRoomNode;
        _graph.EdgeAdded += AddConnectionNode;
        _graph.EdgeRemoved += RemoveConnectionNode;

        foreach (var room in _graph.Vertices) {
            AddRoomNode(room);
        }
        foreach (var connection in _graph.Edges) {
            AddConnectionNode(connection);
        }
    }

    virtual protected void AddRoomNode(Room room) {
        var roomNode = _roomNodePackedScene.Instance() as RoomNode;
        _roomsContainer.AddChild(roomNode);
        roomNode.Type = room.Type;
        roomNode.GlobalPosition = new Vector2(GD.Randi() % 1024, GD.Randi() % 600);
        _roomNodeMap[room] = roomNode;
    }

    virtual protected void RemoveRoomNode(Room room) {
        var roomNode = _roomNodeMap[room];
        _roomNodeMap.Remove(room);
        roomNode.QueueFree();
    }
    virtual protected void AddConnectionNode(Connection connection) {
        var connectionNode = _connectionNodePackedScene.Instance() as ConnectionNode;
        _connectionsContainer.AddChild(connectionNode);
        connectionNode.Type = connection.Type;
        _connectionNodeMap[connection] = connectionNode;
    }

    virtual protected void RemoveConnectionNode(Connection connection) {
        var connectionNode = _connectionNodeMap[connection];
        _connectionNodeMap.Remove(connection);
        connectionNode.QueueFree();
    }

}
