using Godot;
using System;

public class RoomNode : Node2D
{

    public bool Selected = false;

    [Export]
    public string Type {
        get { return GetNode<Label>("TypeLabel").Text; }
        set
        {
            var typeLabel = GetNode<Label>("TypeLabel");
            if (typeLabel.Text != value) {
                typeLabel.Text = value;
            }
        }
    }

    public override void _Draw() {
        var color = Selected ? new Color(0.0f, 0.0f, 1.0f, 1.0f) : new Color(1.0f, 1.0f, 1.0f, 1.0f);
        DrawCircle(new Vector2(0.0f, 0.0f), 25.0f, color);
    }

}
