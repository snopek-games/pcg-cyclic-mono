using Godot;
using System;
using System.Collections.Generic;

public class DetailsDisplay : Node2D
{
    private Label _label;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _label = GetNode<Label>("Panel/Label");
    }

    public void ShowDetails(Dictionary<string, string> attributes, List<PCG.GameObject> gameObjects = null) {
        var lines = new List<string>();

        if (attributes.Count > 0) {  
            lines.Add("Attributes:\n");  
            foreach (var item in attributes) {
                lines.Add(item.Key + ": " + item.Value);
            }
        }

        if (gameObjects != null && gameObjects.Count > 0) {
            lines.Add("Objects:\n");
            foreach (var gameObject in gameObjects) {
                var gameObjectString = gameObject.Type;

                if (gameObject.References.Count > 0) {
                    var referenceStrings = new List<string>();
                    foreach (var item in gameObject.References) {
                        referenceStrings.Add(item.Key + " -> " + item.Value.ToString());
                    }
                    gameObjectString += " (" + String.Join(", ", referenceStrings) + ")";
                }

                lines.Add(gameObjectString);
            }
        }

        _label.Text = String.Join("\n", lines);
    }

}
