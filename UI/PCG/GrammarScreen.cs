using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using PCG;

public class GrammarScreen : GraphBase
{
    private Grammar _grammar;

    private Dictionary<RoomNode, Vector2> _roomNodeForces;

    private PackedScene _plotterScreenPackedScene;

    // Spring rest length.
    private float L = 120.0f;
    // Repulsive force constant.
    private float K_r = 150.0f;
    // Spring force constant.
    private float K_s = .25f;
    // Time step.
    private float delta_t = 2.0f;

    private const float MAX_DISPLACEMENT_SQUARED = 10000.0f;

    private Node2D _selectedRoomNode;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();

        _grammar = new Grammar();
        /*
        _grammar.AddTransformationRule(new PCG.Transformations.ConnectStartToEnd());
        _grammar.AddTransformationRule(new PCG.Transformations.LockEndRoom());
        _grammar.AddTransformationRule(new PCG.Transformations.ExpandBranch());
        */
        _grammar.AddTransformationRule(new TestGrammar1.AddLockedValveLoop());

        _roomNodeForces = new Dictionary<RoomNode, Vector2>();

        _plotterScreenPackedScene = GD.Load("res://UI/PCG/PlotterScreen.tscn") as PackedScene;

        ResetGraph();
    }

    protected override void RepositionRooms() {
        var n = _roomsContainer.GetChildCount();
        var roomNodes = _roomsContainer.GetChildren();

        // Clear all forces.
        foreach (RoomNode node in roomNodes) {
            _roomNodeForces[node] = Vector2.Zero;
        }

        // Repulsion between all nodes.
        for (int i1 = 0; i1 < n - 1; i1++) {
            var node1 = roomNodes[i1] as RoomNode;
            for (int i2 = i1+1; i2 < n; i2++) {
                var node2 = roomNodes[i2] as RoomNode;
                var d = node2.Position - node1.Position;
                if (d.x != 0 || d.y != 0) {
                    var distanceSquared = (d.x * d.x) + (d.y * d.y);
                    var distance = Mathf.Sqrt(distanceSquared);
                    var force = K_r / distanceSquared;
                    var force_vector = force * d / distance;
                    _roomNodeForces[node1] -= force_vector;
                    _roomNodeForces[node2] += force_vector;
                }
            }
        }

        // Spring force between connected nodes.
        foreach (var room in _graph.Vertices) {
            var node1 = _roomNodeMap[room];
            foreach (var connection in _graph.OutEdges(room)) {
                var node2 = _roomNodeMap[connection.Target];
                var d = node2.Position - node1.Position;
                if (d.x != 0 || d.y != 0) {
                    var distance = Mathf.Sqrt((d.x * d.x) + (d.y * d.y));
                    var force = K_s * (distance - L);
                    var force_vector = force * d / distance;
                    _roomNodeForces[node1] += force_vector;
                    _roomNodeForces[node2] -= force_vector;
                }
            }

            // Unrelated hack: make sure displayed type matches graph node.
            node1.Type = room.Type;
        }

        // Update positions.
        foreach (RoomNode node in roomNodes) {
            // Don't update position of the node selected by the user.
            if (node == _selectedRoomNode) {
                continue;
            }

            var d = delta_t * _roomNodeForces[node];
            var displacementSquared = (d.x * d.x) + (d.y * d.y);
            if (displacementSquared > MAX_DISPLACEMENT_SQUARED) {
                var s = Mathf.Sqrt(MAX_DISPLACEMENT_SQUARED / displacementSquared);
                d *= s;
            }
            node.Position += d;
        }
    }

    public override void _Input(InputEvent @event) {
        if (@event is InputEventMouseButton mouseEvent && mouseEvent.ButtonIndex == (int)ButtonList.Left) {
            if (mouseEvent.IsPressed()) {
                _selectedRoomNode = GetClosestNode(_roomsContainer.GetChildren(), mouseEvent.GlobalPosition, 25.0f);
            }
            else {
                _selectedRoomNode = null;
            }
            if (_selectedRoomNode != null) {
                _attributesDisplay.Hide();
            }
        }

        if (@event is InputEventMouseMotion mouseMotion) {
            if (_selectedRoomNode != null) {
                _selectedRoomNode.GlobalPosition = mouseMotion.GlobalPosition;
            }
            else {
                var closestRoomNode = GetClosestNode(_roomsContainer.GetChildren(), mouseMotion.GlobalPosition, 25.0f);
                if (closestRoomNode != null) {
                    var room = _roomNodeMap.FirstOrDefault(x => x.Value == closestRoomNode).Key;
                    ShowDetails(closestRoomNode.GlobalPosition, room.Attributes, room.GameObjects);
                }
                else {
                    var closestConnectionNode = GetClosestNode(_connectionsContainer.GetChildren(), mouseMotion.GlobalPosition, 25.0f);
                    if (closestConnectionNode != null) {
                        ShowDetails(closestConnectionNode.GlobalPosition, _connectionNodeMap.FirstOrDefault(x => x.Value == closestConnectionNode).Key.Attributes);
                    }
                    else {
                        _attributesDisplay.Hide();
                    }
                }
            }
        }
    }

    protected override void AddConnectionNode(Connection connection) {
        base.AddConnectionNode(connection);

        // Attempt to recenter the rooms this refers to.
        RecenterRoomNode(connection.Source);
        RecenterRoomNode(connection.Target);
    }

    protected void RecenterRoomNode(Room room) {
        var roomNode = _roomNodeMap[room];
        var siblingRoomNodes = new List<Node2D>();
        foreach (var connection in _graph.OutEdges(room)) {
            siblingRoomNodes.Add(_roomNodeMap[connection.Target]);
        }
        foreach (var connection in _graph.InEdges(room)) {
            siblingRoomNodes.Add(_roomNodeMap[connection.Source]);
        }
        if (siblingRoomNodes.Count >= 2) {
            var sumPos = Vector2.Zero;
            foreach (var siblingRoomNode in siblingRoomNodes) {
                sumPos += siblingRoomNode.Position;
            }
            roomNode.Position = (sumPos / siblingRoomNodes.Count) + (new Vector2(GD.Randi() % 5, GD.Randi() % 5));
        }
    }

    private void ResetGraph() {
        var newGraph = new Graph();

        var start = new Room("start");
        var end = new Room("end");
        var connection = new Connection(start, end);

        newGraph.AddVertex(start);
        newGraph.AddVertex(end);
        newGraph.AddEdge(connection);

        SetGraph(newGraph);
    }

    public void _on_ResetButton_pressed() {
        ResetGraph();
    }

    public void _on_PlotterButton_pressed() {
        var screenLoader = GetNode<ScreenLoader>("/root/ScreenLoader");
        var plotter = _plotterScreenPackedScene.Instance() as PlotterScreen;
        screenLoader.ShowScreen(plotter);
        var graph = _graph;
        ClearGraph();
        plotter.SetGraph(graph);
    }

    public void _on_TransformButton_pressed() {
        _grammar.Apply(_graph);
    }
}
