using Godot;
using PCG;
using System;
using System.Collections.Generic;

public class GameScreen : Node2D
{

    private Map Map;
    private Player Player;
    private ViewportContainer ViewportContainer;
    private PackedScene _grammarScreenPackedScene;
    private Graph Graph;
    private Dictionary<Room, (int, int)> RoomGridMap;

    public override void _Ready()
    {
        RoomGridMap = new Dictionary<Room, (int, int)>();

        Map = GetNode<Map>("ViewportContainer/Viewport/Map");
        Player = GetNode<Player>("ViewportContainer/Viewport/Player");
        ViewportContainer = GetNode<ViewportContainer>("ViewportContainer");

        GetTree().Root.Connect("size_changed", this, "ResizeViewport");
        ResizeViewport();

        _grammarScreenPackedScene = GD.Load("res://UI/PCG/GrammarScreen.tscn") as PackedScene;
    }

    protected void ResizeViewport() {
        ViewportContainer.RectSize = GetViewportRect().Size;
    }

    public void DrawMap(Graph graph, Room[,] grid) {
        Graph = graph;
        Rect2? startRect = null;

        for (var y = 0; y < grid.GetLength(0); y++) {
            for (var x = 0; x < grid.GetLength(1); x++) {
                var room = grid[y, x];
                if (room == null) {
                    continue;
                }
                RoomGridMap[room] = (x, y);
                Map.DrawRoom(x, y, room);
                if (room.Type == "start") {
                    startRect = Map.GetRoomRect(x, y);
                }
            }
        }

        if (startRect is Rect2 r) {
            Player.GlobalPosition = (r.Position + (r.Size / 2)).Floor() * 8.0f;
        }

        foreach (var connection in graph.Edges) {
            Map.DrawConnection(RoomGridMap[connection.Source], RoomGridMap[connection.Target], connection);
        }

        foreach (var room in graph.Vertices) {
            foreach (var gameObject in room.GameObjects) {
                if (gameObject.Type == "floor_switch") {
                    var (x, y) = RoomGridMap[room];
                    Map.PlaceFloorSwitch(x, y, gameObject.References["unlocks"] as Connection);
                }
            }
        }


    }

    public void _on_StartOverButton_pressed() {
        var screenLoader = GetNode<ScreenLoader>("/root/ScreenLoader");
        var grammarScreen = _grammarScreenPackedScene.Instance() as GrammarScreen;
        screenLoader.ShowScreen(grammarScreen);
    }

}
