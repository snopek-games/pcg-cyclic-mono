using Godot;
using System;
using PCG;

public class PlotterScreen : GraphBase
{
    
    private Graph _originalGraph;
    private Plotter _plotter;
    private int _plotCount = 0;
    private int _iterationCount;
    private Label _iterationCountLabel;
    private PackedScene _grammarScreenPackedScene;
    private PackedScene _gameScreenPackedScene;

    public override void _Ready()
    {
        base._Ready();
        _iterationCountLabel = GetNode<Label>("CanvasLayer/HUD/IterationCountLabel");
        _grammarScreenPackedScene = GD.Load("res://UI/PCG/GrammarScreen.tscn") as PackedScene;
        _gameScreenPackedScene = GD.Load("res://UI/PCG/GameScreen.tscn") as PackedScene;
    }

    public override void SetGraph(Graph newGraph) {
        _originalGraph = newGraph.Clone();
        SetupPlotter(newGraph);
    }

    private void SetupPlotter(Graph newGraph) {
        _plotCount = 0;
        _iterationCount = 0;
        base.SetGraph(newGraph);
        _plotter = new Plotter(newGraph);

    }

    private void UpdateIterationCountLabel() {
        _iterationCountLabel.Text = "Iterations: " + _iterationCount.ToString();
    }

    public override void _Process(float delta) {
        // When set to negative value we want to plot forever.
        if (_plotCount != 0) {
            if (_plotCount > 0) {
                _plotCount--;
            }

            try {
                if (!_plotter.Plot()) {
                    OS.Alert("Grid is fully plotted");
                    _plotCount = 0;
                }
                else {
                    _iterationCount++;
                    UpdateIterationCountLabel();
                }
            }
            catch (UnableToPlotGraphException) {
                OS.Alert("Unable to plot this graph");
                _plotCount = 0;
                // Revert back to the original graph.
                SetupPlotter(_originalGraph);
            }
        }

        base._Process(delta);
    }

    protected override void RepositionRooms() {
        foreach (RoomNode roomNode in _roomsContainer.GetChildren()) {
            roomNode.Visible = false;
            roomNode.Selected = false;
        }

        var (selectedX, selectedY) = _plotter.GetCurrentPosition();

        var grid = _plotter.GetRawGrid();
        for (var y = 0; y < grid.GetLength(0); y++) {
            for (var x = 0; x < grid.GetLength(1); x++) {
                var room = grid[y, x];
                if (room == null) {
                    continue;
                }
                var roomNode = _roomNodeMap[room];
                roomNode.Visible = true;
                roomNode.Position = new Vector2(100 + x * 100, 100 + y * 100);
                if (x == selectedX && y == selectedY) {
                    roomNode.Selected = true;
                }
            }
        }
    }

    private void UpdatePlotCount(int value) {
        if (value == 0 || value == -1) {
            _plotCount = value;
        }
        else {
            if (_plotCount == -1) {
                _plotCount = value;
            }
            else {
                _plotCount += value;
            }
        }
    }

    public void _on_PlotButton_pressed() {
        UpdatePlotCount(1);
    }

    public void _on_PlotButton10_pressed() {
        UpdatePlotCount(10);
    }

    public void _on_PlotButton100_pressed() {
        UpdatePlotCount(100);
    }
    public void _on_PlotButton1000_pressed() {
        UpdatePlotCount(1000);
    }

    public void _on_PlotForeverButton_pressed() {
        _plotCount = -1;
    }

    public void _on_StopButton_pressed() {
        _plotCount = 0;
    }

    public void _on_ResetButton_pressed() {
        SetupPlotter(_originalGraph.Clone());
        UpdateIterationCountLabel();
    }

    public void _on_GameButton_pressed() {
        if (!_plotter.IsFinished()) {
            OS.Alert("Can't switch to game until the graph is fully plotted");
            return;
        }

        var screenLoader = GetNode<ScreenLoader>("/root/ScreenLoader");
        var gameScreen = _gameScreenPackedScene.Instance() as GameScreen;
        screenLoader.ShowScreen(gameScreen);
        gameScreen.DrawMap(_graph, _plotter.GetGrid());
    }

    public void _on_ReturnButton_pressed() {
        var screenLoader = GetNode<ScreenLoader>("/root/ScreenLoader");
        var grammarScreen = _grammarScreenPackedScene.Instance() as GrammarScreen;
        screenLoader.ShowScreen(grammarScreen);
    }

}
