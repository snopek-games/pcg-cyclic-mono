using Godot;
using System;

public class ScreenLoader : Node
{
    private Main Main;

    public override void _Ready()
    {
    }

    public void Setup(Main main) {
        Main = main;
    }

    public void ShowScreen(Node2D newScreen) {
        Node2D oldScreen = Main.GetNode<Node2D>("Screen");
        Main.RemoveChild(oldScreen);
        oldScreen.QueueFree();

        newScreen.Name = "Screen";
        Main.AddChild(newScreen);
    }

}
