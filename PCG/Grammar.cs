using System;
using System.Collections.Generic;
using System.Linq;

namespace PCG {


    public interface ITransformationRule {

        int GetWeight();
        List<Match> FindMatches(Graph map);
        void Execute(Graph graph, Match match);

    }

    public class Grammar {

        protected List<ITransformationRule> TransformationRules;

        protected Random Random;

        public Grammar() {
            TransformationRules = new List<ITransformationRule>();
            Random = new Random();
        }

        public void AddTransformationRule(ITransformationRule rule) {
            TransformationRules.Add(rule);
        }

        // @TODO: We need something to track context with - a dictionary? new class?
        public bool Apply(Graph graph) {
            var applicableRules = new Dictionary<ITransformationRule, List<Match>>();
            List<Match> matches = null;

            foreach (var possibleRule in TransformationRules) {
                matches = possibleRule.FindMatches(graph);
                if (matches.Count > 0) {
                    applicableRules[possibleRule] = matches;
                }
            }

            if (applicableRules.Count == 0) {
                return false;
            }

            // Randomly select an applicable rule.
            var rule = RandomRule(applicableRules.Keys.ToList());
            
            // Randomly select a match.
            matches = applicableRules[rule];
            var matchIndex = Random.Next(0, matches.Count);

            rule.Execute(graph, matches[matchIndex]);

            return true;
        }

        protected ITransformationRule RandomRule(List<ITransformationRule> transformationRules) {
            int totalWeight = transformationRules.Sum(x => x.GetWeight());
            int index = Random.Next(0, totalWeight);

            ITransformationRule selectedRule = null;

            foreach (var rule in transformationRules) {
                int ruleWeight = rule.GetWeight();
                if (index < ruleWeight) {
                    selectedRule = rule;
                    break;
                }
                index -= ruleWeight;
            }

            return selectedRule;
        }

    }

    namespace Transformations {
        public class ConnectStartToEnd : ITransformationRule {

            public int GetWeight() {
                return 100;
            }

            public List<Match> FindMatches(Graph graph) {
                var constraints = new Constraints();
                constraints.AddRoomConstraint("start", new RoomConstraint(new StringConstraint("start")));
                constraints.AddRoomConstraint("end", new RoomConstraint(new StringConstraint("end")));
                constraints.AddConnectionConstraint("connection", new ConnectionConstraint("start", "end"));
                return graph.FindMatches(constraints);
            }

            public void Execute(Graph graph, Match match) {
                graph.RemoveEdge(match.Connections["connection"]);
                var path1 = new Room();
                var path2 = new Room();
                var connection1 = new Connection(match.Rooms["start"], path1);
                var connection2 = new Connection(match.Rooms["start"], path2);
                var connection3 = new Connection(path1, match.Rooms["end"]);
                var connection4 = new Connection(path2, match.Rooms["end"]);
                graph.AddVertex(path1);
                graph.AddVertex(path2);
                graph.AddEdge(connection1);
                graph.AddEdge(connection2);
                graph.AddEdge(connection3);
                graph.AddEdge(connection4);
            }

        }

        public class LockEndRoom : ITransformationRule {

            public int GetWeight() {
                return 100;
            }

            public List<Match> FindMatches(Graph graph) {
                var constraints = new Constraints();
                var not_start_constraint = new StringConstraint();
                not_start_constraint.AddDisallowedValue("start");
                constraints.AddRoomConstraint("room", new RoomConstraint(not_start_constraint));
                constraints.AddRoomConstraint("end", new RoomConstraint(new StringConstraint("end")));
                constraints.AddConnectionConstraint("connection", new ConnectionConstraint("room", "end", new StringConstraint("open")));
                return graph.FindMatches(constraints);
            }

            public void Execute(Graph graph, Match match) {
                match.Rooms["end"].Type = "empty";
                var newEnd = new Room("end");
                var connection = new Connection(match.Rooms["end"], newEnd, "locked");

                graph.AddVertex(newEnd);
                graph.AddEdge(connection);

                var floorSwitch = new GameObject("switch");
                floorSwitch.References["unlocks"] = connection;
                match.Rooms["room"].GameObjects.Add(floorSwitch);

                // @temp: for testing attributes display:
                //match.Rooms["room"].Attributes["has_switch"] = "yep";
                //connection.Attributes["swichable"] = "you know it!";
                //connection.Attributes["a 2nd attribute"] = "for testing, you know";
            }

        }

        public class ExpandBranch : ITransformationRule {
            
            public int GetWeight() {
                return 10;
            }

            public List<Match> FindMatches(Graph graph) {
                var constraints = new Constraints();
                var type_constraint = new StringConstraint();
                type_constraint.AddDisallowedValue("start");
                type_constraint.AddDisallowedValue("end");
                constraints.AddRoomConstraint("room1", new RoomConstraint(type_constraint));
                constraints.AddRoomConstraint("room2", new RoomConstraint(type_constraint));
                constraints.AddConnectionConstraint("connection", new ConnectionConstraint("room1", "room2", new StringConstraint("open")));
                return graph.FindMatches(constraints);
            }

            public void Execute(Graph graph, Match match) {
                graph.RemoveEdge(match.Connections["connection"]);
                var newRoom = new Room();
                var connection1 = new Connection(match.Rooms["room1"], newRoom);
                var connection2 = new Connection(newRoom, match.Rooms["room2"]);
                graph.AddVertex(newRoom);
                graph.AddEdge(connection1);
                graph.AddEdge(connection2);
            }

        }
    }
}