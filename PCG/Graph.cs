using System;
using System.Collections.Generic;
using System.Linq;
using QuikGraph;

namespace PCG {
    
    public class GameObject {
        public string Type { get; set; }

        public Dictionary<string, object> References { get; }

        public GameObject(string type) {
            Type = type;
            References = new Dictionary<string, object>();
        }
    }

    public class Room {
        public string Type { get; set; }
        public Dictionary<string, string> Attributes { get; }
        public List<GameObject> GameObjects { get; }

        public Room(string type = "empty") {
            Type = type;
            Attributes = new Dictionary<string, string>();
            GameObjects = new List<GameObject>();
        }

        public override string ToString() {
            return base.ToString() + ": " + Type;
        }
    }

    public class Connection : Edge<Room> {
        public string Type { get; set; }
        public Dictionary<string, string> Attributes { get; }

        public Connection(Room a, Room b, string type = "open", Dictionary<string, string> attributes = null) : base(a, b) {
            Type = type;
            if (attributes == null) {
                attributes = new Dictionary<string, string>();
            }
            Attributes = attributes;
        }

        public Connection Copy(Room source, Room target) {
            return new Connection(source, target, Type, new Dictionary<string, string>(Attributes));
        }

    }

    public class StringConstraint {

        protected List<string> AllowedValues;
        protected List<string> DisallowedValues;

        public StringConstraint(List<string> allowedValues = null, List<string> disallowedValues = null) {
            if (allowedValues == null) {
                allowedValues = new List<string>();
            }
            AllowedValues = allowedValues;
            if (disallowedValues == null) {
                disallowedValues = new List<string>();
            }
            DisallowedValues = disallowedValues;
        }

        public StringConstraint(string s) : this(new List<string> { s }) { }

        public void AddAllowedValue(string s) {
            AllowedValues.Add(s);
        }

        public void AddDisallowedValue(string s) {
            DisallowedValues.Add(s);
        }

        public bool Matches(string s) {
            if (AllowedValues.Count > 0 && !AllowedValues.Contains("*")) {
                bool found = false;
                foreach (var allowed in AllowedValues) {
                    if (s == allowed) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }

            foreach (var disallowed in DisallowedValues) {
                if (s == disallowed) {
                    return false;
                }
            }

            return true;
        }

        public static bool MatchesDictionary(Dictionary<string, StringConstraint> constraints, Dictionary<string, string> strings) {
            foreach (var item in constraints) {
                if (!strings.ContainsKey(item.Key)) {
                    return false;
                };
                if (!item.Value.Matches(strings[item.Key])) {
                    return false;
                }
            }
            return true;
        }

    }

    public class RoomConstraint {
        public StringConstraint Type { get; set; }
        public Dictionary<string, StringConstraint> Attributes { get; }
        public int MaxConnections { get; set; }

        public RoomConstraint(StringConstraint type = null, Dictionary<string, StringConstraint> attributes = null) {
            Type = type;
            if (attributes == null) {
                attributes = new Dictionary<string, StringConstraint>();
            }
            Attributes = attributes;
            MaxConnections = -1;
        }

        public bool Matches(Room room) {
            return (Type == null || Type.Matches(room.Type)) && StringConstraint.MatchesDictionary(Attributes, room.Attributes);
        }

    }

    public class ConnectionConstraint {

        public string Source { get; set; }
        public string Target { get; set; }
        public StringConstraint Type { get; set; }
        public bool Bidirectional { get; set; }
        public Dictionary<string, StringConstraint> Attributes { get; }

        public ConnectionConstraint(string source, string target, StringConstraint type = null, Dictionary<string, StringConstraint> attributes = null) {
            Source = source;
            Target = target;
            Type = type;
            Bidirectional = false;
            if (attributes == null) {
                attributes = new Dictionary<string, StringConstraint>();
            }
            Attributes = attributes;
        }

        public bool Matches(Connection connection) {
            return (Type == null || Type.Matches(connection.Type)) && StringConstraint.MatchesDictionary(Attributes, connection.Attributes);
        }
    }

    public class InvalidConstraintsException : Exception {
        public InvalidConstraintsException(string message) : base(message) { }
    }

    public class Constraints {

        protected Dictionary<string, RoomConstraint> RoomConstraints;
        protected Dictionary<string, ConnectionConstraint> ConnectionConstraints;

        public Constraints() {
            RoomConstraints = new Dictionary<string, RoomConstraint>();
            ConnectionConstraints = new Dictionary<string, ConnectionConstraint>();
        }

        public void AddRoomConstraint(string name, RoomConstraint constraint) {
            RoomConstraints[name] = constraint;
        }

        public void AddConnectionConstraint(string name, ConnectionConstraint constraint) {
            ConnectionConstraints[name] = constraint;
        }

        public void Validate() {
            if (RoomConstraints.Count == 0) {
                throw new InvalidConstraintsException("Must have at least one RoomConstraint");
            }
            foreach (var item in ConnectionConstraints) {
                if (!RoomConstraints.ContainsKey(item.Value.Source)) {
                    throw new InvalidConstraintsException($"ConnectionConstraint '{item.Key}' has source '{item.Value.Source}' without RoomConstraint");
                }
                if (!RoomConstraints.ContainsKey(item.Value.Target)) {
                    throw new InvalidConstraintsException($"ConnectionConstraint '{item.Key}' has target '{item.Value.Source}' without RoomConstraint");
                }
            }
            _ValidateAllRoomsAreConnected();
        }

        private void _ValidateAllRoomsAreConnected() {
            var connectedRooms = new Dictionary<string, bool>();
            var firstRoomName = RoomConstraints.Keys.First();
            connectedRooms[firstRoomName] = true;
            _FindConnectedRooms(firstRoomName, connectedRooms);
            var unconnected = RoomConstraints.Keys.Except(connectedRooms.Keys).ToList();
            if (unconnected.Count > 0) {
                throw new InvalidConstraintsException($"The following room constraints don't appear to be connected to the others: {String.Join(", ", unconnected)}");
            }
        }

        private void _FindConnectedRooms(string roomName, Dictionary<string, bool> connectedRooms) {
            foreach (var connection_constraint in ConnectionConstraints.Values) {
                if (connection_constraint.Source == roomName || connection_constraint.Target == roomName) {
                    if (!connectedRooms.ContainsKey(connection_constraint.Source)) {
                        connectedRooms[connection_constraint.Source] = true;
                        _FindConnectedRooms(connection_constraint.Source, connectedRooms);
                    }
                    if (!connectedRooms.ContainsKey(connection_constraint.Target)) {
                        connectedRooms[connection_constraint.Target] = true;
                        _FindConnectedRooms(connection_constraint.Target, connectedRooms);
                    }
                }
            }
        }

        public Match FindMatch(Graph graph, Room room, bool validate = true) {
            if (validate) {
                Validate();
            }
            
            var match = new Match();
            var firstRoomName = RoomConstraints.Keys.First();
            return _MatchRoom(graph, room, firstRoomName, match);
        }

        protected Dictionary<string, ConnectionConstraint> GetRoomConnectionConstraints(string roomName) {
            var result = new Dictionary<string, ConnectionConstraint>();
            foreach (var connectionConstraintItem in ConnectionConstraints) {
                if (connectionConstraintItem.Value.Source == roomName || connectionConstraintItem.Value.Target == roomName) {
                    result[connectionConstraintItem.Key] = connectionConstraintItem.Value;
                }
            }
            return result;
        }

        protected bool _CheckConnectionDirection(ConnectionConstraint constraint, Connection connection, string roomName, Room room) {
            if (constraint.Bidirectional) {
                return true;
            }
            return constraint.Source == roomName ? connection.Source == room : connection.Target == room;
        }

        protected Match _MatchRoom(Graph graph, Room room, string roomName, Match match) {
            var roomConstraint = RoomConstraints[roomName];
            if (!roomConstraint.Matches(room)) {
                return null;
            }

            if (roomConstraint.MaxConnections >= 0) {
                var connectionCount = graph.InEdges(room).Count() + graph.OutEdges(room).Count();
                if (connectionCount > roomConstraint.MaxConnections) {
                    return null;
                }

            }

            // If we've already matched this room, then don't recurse on the connections again.
            if (match.Rooms.ContainsKey(roomName)) {
                return match;
            }

            match.Rooms[roomName] = room;

            foreach (var connectionConstraintItem in GetRoomConnectionConstraints(roomName)) {
                var otherRoomName = connectionConstraintItem.Value.Source == roomName ? connectionConstraintItem.Value.Target : connectionConstraintItem.Value.Source;

                bool found = false;
                foreach (var connection in graph.OutEdges(room)) {
                    if (_CheckConnectionDirection(connectionConstraintItem.Value, connection, roomName, room) && connectionConstraintItem.Value.Matches(connection)) {
                        var otherMatch = _MatchRoom(graph, connection.Target, otherRoomName, match);
                        if (otherMatch != null) {
                            match = otherMatch;
                            found = true;
                            match.Connections[connectionConstraintItem.Key] = connection;
                            break;
                        }
                    }
                }
                if (!found) {
                    foreach (var connection in graph.InEdges(room)) {
                        if (_CheckConnectionDirection(connectionConstraintItem.Value, connection, roomName, room) && connectionConstraintItem.Value.Matches(connection)) {
                            var otherMatch = _MatchRoom(graph, connection.Source, otherRoomName, match);
                            if (otherMatch != null) {
                                match = otherMatch;
                                found = true;
                                match.Connections[connectionConstraintItem.Key] = connection;
                                break;
                            }
                        }
                    }   
                }
                if (!found) {
                    return null;
                }
            }

            return match;
        }
    
    }

    public class Match {

        public Dictionary<string, Room> Rooms { get; }
        public Dictionary<string, Connection> Connections { get; }

        public Match(Dictionary<string, Room> rooms = null, Dictionary<string, Connection> connections = null) {
            if (rooms == null) {
                rooms = new Dictionary<string, Room>();
            }
            Rooms = rooms;

            if (connections == null) {
                connections = new Dictionary<string, Connection>();
            }
            Connections = connections;
        }

    }


    public class Graph : BidirectionalGraph<Room, Connection> {

        public List<Match> FindMatches(Constraints constraints) {
            constraints.Validate();

            var result = new List<Match>();
            foreach (var room in Vertices) {
                var match = constraints.FindMatch(this, room, false);
                if (match != null) {
                    result.Add(match);
                }
            }
            return result;
        }

        public new Graph Clone() {
            var newGraph = new Graph();
            foreach (var vertex in Vertices) {
                newGraph.AddVertex(vertex);
            }
            foreach (var edge in Edges) {
                newGraph.AddEdge(edge);
            }
            return newGraph;
        }

    }

}