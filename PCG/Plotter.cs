using System;
using System.Collections.Generic;
using System.Linq;

namespace PCG {

    public class UnableToPlotGraphException : Exception { }

    public class Plotter {
        protected struct Vector2i {
            public int X;
            public int Y;

            public Vector2i(int x = 0, int y = 0) {
                X = x;
                Y = y;
            }

            public Vector2i Abs() {
                return new Vector2i(Math.Abs(X), Math.Abs(Y));
            }

            public override string ToString() {
                return X.ToString() + ", " + Y.ToString();
            }

            public static Vector2i operator +(Vector2i a, Vector2i b) =>
                new Vector2i(a.X + b.X, a.Y + b.Y);
            
            public static Vector2i operator -(Vector2i a, Vector2i b) =>
                new Vector2i(a.X - b.X, a.Y - b.Y);
            
            public static bool operator ==(Vector2i a, Vector2i b) =>
                a.X == b.X && a.Y == b.Y;

            public static bool operator !=(Vector2i a, Vector2i b) =>
                a.X != b.X && a.Y != b.Y;
            
            public override bool Equals(object obj) {
                if (obj is Vector2i v)
                {
                    return X == v.X && Y == v.Y;
                }
                return false;
            }

            public override int GetHashCode() {
                return (X, Y).GetHashCode();
            }

            public static readonly Vector2i UP = new Vector2i(0, -1);
            public static readonly Vector2i DOWN = new Vector2i(0, 1);
            public static readonly Vector2i LEFT = new Vector2i(-1, 0);
            public static readonly Vector2i RIGHT = new Vector2i(1, 0);

            public static readonly Vector2i[] DIRECTIONS;

            static Vector2i() {
                DIRECTIONS = new Vector2i[4] {
                    Vector2i.UP,
                    Vector2i.DOWN,
                    Vector2i.LEFT,
                    Vector2i.RIGHT,
                };
            }
        }

        protected class State : ICloneable {
            protected Plotter Plotter;

            public List<Room[]> Grid { get; }
            public Dictionary<Room, bool> AlreadyVisited { get; }
            public Dictionary<Room, Room> RollbackAlternativePaths { get; }

            public State(Plotter plotter) {
                Plotter = plotter;
                Grid = new List<Room[]>();
                AlreadyVisited = new Dictionary<Room, bool>();
                RollbackAlternativePaths = new Dictionary<Room, Room>();
            }

            public object Clone() {
                var newState = new State(Plotter);

                foreach (var row in Grid) {
                    newState.Grid.Add(row.Clone() as Room[]);
                }

                foreach (var visitedItem in AlreadyVisited) {
                    newState.AlreadyVisited[visitedItem.Key] = visitedItem.Value;
                }

                foreach (var pathItem in RollbackAlternativePaths) {
                    newState.RollbackAlternativePaths[pathItem.Key] = pathItem.Value;
                }

                return newState;
            }
            public void AddGridRow() {
                Grid.Add(new Room[Plotter.Width]);
            }

            public Room GetRoom(Vector2i pos) {
                if (pos.X < 0 || pos.X >= Plotter.Width || pos.Y < 0 || pos.Y >= Grid.Count) {
                    return null;
                }
                return Grid[pos.Y][pos.X];
            }

            public Vector2i? GetPosition(Room room) {
                int y = 0;
                foreach (var row in Grid) {
                    var x = 0;
                    foreach (var spot in row) {
                        if (spot == room) {
                            return new Vector2i(x, y);
                        }
                        x++;
                    }
                    y++;
                }
                return null;
            }


            private void RollbackAlternativePath(Room path) {
                var path_pos = GetPosition(path);
                if (path_pos != null) {
                    UnplotRoom((Vector2i)path_pos);
                }
                Plotter.RemoveAlternativePath(path);

            }

            public void UnplotRoom(Vector2i pos) {
                var room = Grid[pos.Y][pos.X];
                if (room != null) {
                    Grid[pos.Y][pos.X] = null;
                    AlreadyVisited.Remove(room);

                    if (RollbackAlternativePaths.ContainsKey(room)) {
                        RollbackAlternativePath(RollbackAlternativePaths[room]);
                    }
                }
            }

            public List<Room> GetMissingAlternativePaths(State oldState) {
                var missing = new List<Room>();
                foreach (var item in RollbackAlternativePaths) {
                    if (!oldState.RollbackAlternativePaths.ContainsKey(item.Key)) {
                        missing.Add(item.Value);
                    }
                }
                return missing;
            }

            public void PlotRoom(Room current, Vector2i pos) {
                while (pos.Y >= Grid.Count) {
                    AddGridRow();
                }
                Grid[pos.Y][pos.X] = current;
                AlreadyVisited[current] = true;
            }
            public bool IsSpotAvailable(Vector2i pos) {
                if (pos.Y >= Grid.Count) {
                    return true;
                }
                if (pos.Y < 0 || pos.X < 0 || pos.X >= Plotter.Width) {
                    return false;
                }
                return Grid[pos.Y][pos.X] == null;
            }

            public void AddIfAvailable(List<Vector2i> list, Vector2i pos) {
                if (IsSpotAvailable(pos)) {
                    list.Add(pos);
                }
            }

            public List<Vector2i> GetAvailableSpots(Vector2i pos) {
                var available = new List<Vector2i>();
                AddIfAvailable(available, pos + Vector2i.DOWN);
                AddIfAvailable(available, pos + Vector2i.RIGHT);
                AddIfAvailable(available, pos + Vector2i.LEFT);
                AddIfAvailable(available, pos + Vector2i.UP);
                return available;
            }

            public List<Room> FindMissingNeighbors(Vector2i position, List<Room> neighbors) {
                var missingNeighbors = new List<Room>();
                foreach (var neighbor in neighbors) {
                    bool found = false;
                    foreach (var direction in Vector2i.DIRECTIONS) {
                        if (GetRoom(position + direction) == neighbor) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        missingNeighbors.Add(neighbor);
                    }
                }
                return missingNeighbors;
            }

            // Take the finished grid, and generate a compact array out of it.
            public Room[,] TruncateGrid() {
                int minX = Plotter.Width, maxX = 0, minY = Grid.Count, maxY = 0;
                bool foundSomething = false;

                for (var y = 0; y < Grid.Count; y++) {
                    for (var x = 0; x < Plotter.Width; x++) {
                        if (Grid[y][x] != null) {
                            foundSomething = true;
                            minX = Math.Min(x, minX);
                            maxX = Math.Max(x, maxX);
                            minY = Math.Min(y, minY);
                            maxY = Math.Max(y, maxY);
                        }
                    }
                }

                if (!foundSomething) {
                    return new Room[0, 0];
                }

                var result = new Room[(maxY - minY) + 1, (maxX - minX) + 1];
                
                for (var y = 0; y < Grid.Count; y++) {
                    for (var x = 0; x < Plotter.Width; x++) {
                        if (Grid[y][x] != null) {
                            result[y - minY, x - minX] = Grid[y][x];
                        }
                    }
                }
                
                return result;
            }

        }

        protected class StackFrame {

            public int ParentIndex { get; }
            public State OriginalState { get; }

            public Room CurrentRoom { get; }

            public Vector2i Position { get; }

            private Queue<(Room, Vector2i)[]> NeighborPermutationQueue;
            private Queue<(Room, Vector2i)> NeighborQueue;

            public StackFrame(int parentFrame, State originalState, Room currentRoom, Vector2i position, List<Room> neighbors, List<Vector2i> availableSpots) {
                ParentIndex = parentFrame;
                OriginalState = originalState;
                CurrentRoom = currentRoom;
                Position = position;
                if (neighbors.Count > 0) {
                    NeighborPermutationQueue = GeneratePermutations(originalState, position, neighbors, availableSpots);
                    NeighborQueue = new Queue<(Room, Vector2i)>(NeighborPermutationQueue.Dequeue());
                }
            }

            private class PermutationComparer : IEqualityComparer<(Room, Vector2i)[]> {
                public bool Equals((Room, Vector2i)[] a, (Room, Vector2i)[] b) {
                    if (a.Length != b.Length) {
                        return false;
                    }
                    for (var i = 0; i < a.Length; i++) {
                        if (a[i].Item1 != b[i].Item1 || a[i].Item2 != b[i].Item2) {
                            return false;
                        }
                    }
                    return true;
                }

                public int GetHashCode((Room, Vector2i)[] a) {
                    // Stole this hash algorithm from here:
                    //   https://stackoverflow.com/a/8094931
                    int hash = 19;
                    foreach (var t in a) {
                        hash = hash * 31 + t.GetHashCode();
                    }
                    return hash;
                }
            }

            private static Queue<(Room, Vector2i)[]> GeneratePermutations(State state, Vector2i position, List<Room> neighbors, List<Vector2i> availableSpotsMaster) {
                var result = new List<(Room, Vector2i)[]>();

                foreach (var availableSpots in Plotter.GetPermutations(availableSpotsMaster.ToArray())) {
                    var availableSpotsQueue = new Queue<Vector2i>(availableSpots);
                    var pairs = new List<(Room, Vector2i)>();

                    foreach (var neighbor in neighbors) {
                        pairs.Add((neighbor, availableSpotsQueue.Dequeue()));
                    }

                    result.Add(pairs.ToArray());
                }

                foreach (var pairs in result.ToArray()) {
                    foreach (var pairPermutation in Plotter.GetPermutations(pairs)) {
                        result.Add(pairPermutation);
                    }
                }

                return new Queue<(Room, Vector2i)[]>(result.Distinct(new PermutationComparer()));
            }

            public (Room, Vector2i) GetNextNeighbor() {
                if (NeighborQueue == null) {
                    throw new InvalidOperationException();
                }
                return NeighborQueue.Dequeue();
            }

            public bool TryNextPermutation() {
                if (NeighborPermutationQueue == null) {
                    return false;
                }

                try {
                    NeighborQueue = new Queue<(Room, Vector2i)>(NeighborPermutationQueue.Dequeue());
                }
                catch(InvalidOperationException) {
                    NeighborQueue = null;
                    return false;
                }
                return true;
            }

        }

        protected Graph Graph;
        protected int Width;
        protected State CurrentState;
        protected List<StackFrame> Stack;
        protected int CurrentStackFrame = -1;

        public Plotter(Graph graph) {
            Graph = graph;
            Width = graph.Vertices.Count() * 2;
            CurrentState = new State(this);
            Stack = new List<StackFrame>();
            Setup();
        }

        protected Room FindEndRoom() {
            var rooms =
                (from v in Graph.Vertices
                where v.Type == "end"
                select v).ToArray();
            return rooms[0];
        }

        protected void Setup() {
            var end = FindEndRoom();
            var pos = new Vector2i
            {
                Y = 0,
                X = Width / 2
            };
            DoPlotRoom(end, pos);
            CurrentStackFrame = 0;
        }

        protected bool DoPlotRoom(Room currentRoom, Vector2i pos) {
            if (CurrentState.AlreadyVisited.ContainsKey(currentRoom)) {
                return true;
            }

            if (!CurrentState.IsSpotAvailable(pos)) {
                return false;
            }

            var neighbors = GetNeighbors(currentRoom, true);
            var availableSpots = CurrentState.GetAvailableSpots(pos);

            if (neighbors.Count > availableSpots.Count) {
                return false;
            }

            var originalState = SaveState();
            CurrentState.PlotRoom(currentRoom, pos);

            var newStackFrame = new StackFrame(CurrentStackFrame, originalState, currentRoom, pos, neighbors, availableSpots);
            if (CurrentStackFrame == Stack.Count - 1) {
                Stack.Add(newStackFrame);
            }
            else {
                Stack.Insert(CurrentStackFrame + 1, newStackFrame);
            }

            return true;
        }

        protected void TryNextPermutation() {
            var stackFrame = Stack[CurrentStackFrame];
            Stack.RemoveRange(stackFrame.ParentIndex + 1, Stack.Count - (stackFrame.ParentIndex + 1));
            RestoreState(stackFrame.OriginalState);

            if (Stack.Count == 0) {
                throw new UnableToPlotGraphException();
            }

            CurrentStackFrame = stackFrame.ParentIndex;
            if (CurrentStackFrame < 0) {
                throw new UnableToPlotGraphException();
            }
            Stack[CurrentStackFrame].TryNextPermutation();
        }

        protected bool EnsureNoMissingNeighbors(Room room, Vector2i pos) {
            var missingNeighbors = CurrentState.FindMissingNeighbors(pos, GetNeighbors(room));
            if (missingNeighbors.Count > 0) {
                if (PlotAlternatePathToNeighbors(room, pos, missingNeighbors)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            return true;
        }

        public bool IsFinished() {
            return Stack.Count == 0;
        }

        public bool Plot() {
            if (IsFinished()) {
                return false;
            }

            var stackFrame = Stack[CurrentStackFrame];
            Room neighbor;
            Vector2i availableSpot;

            while (true) {
                try {
                    (neighbor, availableSpot) = stackFrame.GetNextNeighbor();
                    if (!DoPlotRoom(neighbor, availableSpot)) {
                        TryNextPermutation();
                        return true;
                    }
                }
                catch (InvalidOperationException) {
                    break;
                }
            }

            // If we are at the top of the stack (ie. we didn't add some neighbors
            // to come after us in the stack in the loop above), then we ensure
            // there are no missing neighbors, and then move back up the stack.
            if (CurrentStackFrame == Stack.Count - 1) {
                if (EnsureNoMissingNeighbors(stackFrame.CurrentRoom, stackFrame.Position)) {
                    // We'll move back up to the parent normally.
                    Stack.RemoveAt(CurrentStackFrame--);
                }
                else {
                    TryNextPermutation();
                }
                return true;
            }
            else {
                // If we aren't at the top stack, then we need to ascend the stack
                // to address our first neighbor.
                CurrentStackFrame++;
            }

            return true;
        }

        public Room[,] GetGrid() {
            return CurrentState.TruncateGrid();
        }

        public Room[,] GetRawGrid() {
            var grid = CurrentState.Grid;
            var result = new Room[grid.Count, Width];
            for (int y = 0; y < grid.Count; y++) {
                for (int x = 0; x < Width; x++) {
                    result[y, x] = grid[y][x];
                }
            }
            return result;
        }

        public (int, int) GetCurrentPosition() {
            if (Stack.Count > 0) {
                var stackFrame = Stack[CurrentStackFrame];
                return (stackFrame.Position.X, stackFrame.Position.Y);
            }
            return (0, 0);
        }

        public static void ApplyAttributesToGrid(Room[,] grid) {
            for (var y = 0; y < grid.GetLength(0); y++) {
                for (var x = 0; x < grid.GetLength(1); x++) {
                    var room = grid[y, x];
                    if (room == null) {
                        continue;
                    }
                    room.Attributes["grid_x"] = x.ToString();
                    room.Attributes["grid_y"] = y.ToString();
                }
            }
        }

        protected State SaveState() {
            return CurrentState.Clone() as State;
        }

        protected void RestoreState(State oldState) {
            foreach (var path in CurrentState.GetMissingAlternativePaths(oldState)) {
                RemoveAlternativePath(path);
            }
            CurrentState = oldState;
        }

        protected List<Room> GetNeighbors(Room current, bool excludeAlreadyVisited = false) {
            var neighbors = new List<Room>();
            foreach (var connection in Graph.OutEdges(current)) {
                if (!neighbors.Contains(connection.Target) && !(excludeAlreadyVisited && CurrentState.AlreadyVisited.ContainsKey(connection.Target))) {
                    neighbors.Add(connection.Target);
                }
            }
            foreach (var connection in Graph.InEdges(current)) {
                if (!neighbors.Contains(connection.Source) && !(excludeAlreadyVisited && CurrentState.AlreadyVisited.ContainsKey(connection.Source))) {
                    neighbors.Add(connection.Source);
                }
            }
            return neighbors;
        }

        private static IEnumerable<T[]> GetPermutations<T>(T[] values) {
            if (values.Length == 1)
                return new[] {values};

            return values.SelectMany(v => GetPermutations(values.Except(new[] {v}).ToArray()),
                (v, p) => new[] {v}.Concat(p).ToArray());
        }

        virtual protected bool PlotAlternatePathToNeighbors(Room current, Vector2i pos, List<Room> missingNeighbors) {
            // Our default algorithm can only handle a single missing neighbor.
            if (missingNeighbors.Count > 1) {
                return false;
            }

            var neighbor = missingNeighbors[0];
            var neighborPosition = CurrentState.GetPosition(neighbor);
            if (neighborPosition == null) {
                return false;
            }

            var positionDifference = (neighborPosition.Value - pos);

            // Determine if the neighbor is diagonal from this room's position.
            if (positionDifference.Abs() == new Vector2i(1, 1)) {
                var available = new List<Vector2i>();
                CurrentState.AddIfAvailable(available, pos + (new Vector2i(positionDifference.X, 0)));
                CurrentState.AddIfAvailable(available, pos + (new Vector2i(0, positionDifference.Y)));
                if (available.Count == 0) {
                    return false;
                }

                return PlotAlternativePath(current, neighbor, available[0]);
            }

            return false;
        }

        virtual protected bool PlotAlternativePath(Room current, Room neighbor, Vector2i pos) {
            Connection connection;

            if (Graph.TryGetEdge(current, neighbor, out connection)) {
                var path = CreateAlternativePath(current, neighbor, connection);
                CurrentState.PlotRoom(path, pos);
                CurrentState.RollbackAlternativePaths[current] = path;
                return true;
            }

            if (Graph.TryGetEdge(neighbor, current, out connection)) {
                var path = CreateAlternativePath(current, neighbor, connection);
                CurrentState.PlotRoom(path, pos);
                CurrentState.RollbackAlternativePaths[current] = path;
                return true;
            }

            return false;
        }

        virtual protected Room CreateAlternativePath(Room current, Room neighbor, Connection connection) {
            Room source;
            Room target;
            if (connection.Source == current) {
                source = current;
                target = neighbor;
            }
            else {
                source = neighbor;
                target = current;
            }

            var path = new Room("path");
            var pathToTarget = connection.Copy(path, target);
            var sourceToPath = new Connection(source, path);

            Graph.AddVertex(path);
            Graph.RemoveEdge(connection);
            Graph.AddEdge(pathToTarget);
            Graph.AddEdge(sourceToPath);

            return path;
        }

        virtual protected void RemoveAlternativePath(Room path) {
            foreach(var inConnection in Graph.InEdges(path)) {
                foreach (var outConnection in Graph.OutEdges(path)) {
                    var newConnection = outConnection.Copy(inConnection.Source, outConnection.Target);
                    Graph.AddEdge(newConnection);
                }
            }

            Graph.RemoveVertex(path);
        }


    }
}