using Godot;
using System;

public class Main : Node2D
{
    public override void _Ready()
    {
        var screenLoader = GetNode<ScreenLoader>("/root/ScreenLoader");
        screenLoader.Setup(this);
    }

}
