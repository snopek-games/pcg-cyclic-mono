using Godot;
using System;

public class Player : KinematicBody2D
{
    [Export]
    public float Speed = 200.0f;

    public override void _PhysicsProcess(float delta) {
        var vector = new Vector2();

        if (Input.IsActionJustPressed("player_up")) {
            vector.y -= 1;
        }
        if (Input.IsActionJustPressed("player_down")) {
            vector.y += 1;
        }
        if (Input.IsActionJustPressed("player_left")) {
            vector.x -= 1;
        }
        if (Input.IsActionJustPressed("player_right")) {
            vector.x += 1;
        }

        MoveAndCollide(vector * 8.0f);
    }

}
