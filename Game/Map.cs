using Godot;
using System;
using System.Collections.Generic;
using PCG;

public class Map : Node2D
{
    private TileMap TileMap;

    private Node2D _objectsContainer;
    private PackedScene _goalPackedScene;
    private PackedScene _floorSwitchPackedScene;
    private PackedScene _doorPackedScene;

    private Dictionary<Connection, Door> ConnectionDoorMap;

    public override void _Ready()
    {
        TileMap = GetNode<TileMap>("TileMap");
        TileMap.Clear();

        _objectsContainer = GetNode<Node2D>("Objects");
        _goalPackedScene = GD.Load("res://Game/Goal.tscn") as PackedScene;
        _floorSwitchPackedScene = GD.Load("res://Game/FloorSwitch.tscn") as PackedScene;
        _doorPackedScene = GD.Load("res://Game/Door.tscn") as PackedScene;

        ConnectionDoorMap = new Dictionary<Connection, Door>();
    }

    public Vector2 GetRoomPosition(int gridX, int gridY) {
        return new Vector2((gridX * 16) + 5, (gridY * 16) + 5);
    }

    public Rect2 GetRoomRect(int gridX, int gridY) {
        return new Rect2(GetRoomPosition(gridX, gridY), new Vector2(11, 11));
    }

    public void DrawRoom(int gridX, int gridY, Room room) {
        var rect = GetRoomRect(gridX, gridY);
        var start = rect.Position;
        var end = rect.Position + rect.Size - new Vector2(1, 1);

        for (int y = (int)start.y; y < end.y + 1; y++) {
            for (int x = (int)start.x; x < end.x + 1; x++) {
                string tileName = "Empty";

                if (y == start.y) {
                    if (x == start.x) {
                        tileName = "TopLeft";
                    }
                    else if (x == end.x) {
                        tileName = "TopRight";
                    }
                    else {
                        tileName = "Top";
                    }
                }
                else if (y == end.y) {
                    if (x == start.x) {
                        tileName = "BottomLeft";
                    }
                    else if (x == end.x) {
                        tileName = "BottomRight";
                    }
                    else {
                        tileName = "Top";
                    }
                }
                else if (x == start.x || x == end.x) {
                    tileName = "SideWall";
                }

                TileMap.SetCell(x, y, TileMap.TileSet.FindTileByName(tileName));
            }
        }

        if (room.Type == "end") {
            PlaceGoal(rect);
        }
    }

    public void DrawConnection((int x, int y) source, (int x, int y) target, Connection connection) {
        var sourceRect = GetRoomRect(source.x, source.y);
        var targetRect = GetRoomRect(target.x, target.y);

        if (source.x == target.x) {
            var start = (source.y < target.y ? sourceRect.Position : targetRect.Position)
                + new Vector2(Mathf.Floor(sourceRect.Size.x / 2), sourceRect.Size.y - 1);
            for (int y = 0; y < 7; y++) {
                TileMap.SetCell((int)start.x, (int)start.y + y, TileMap.TileSet.FindTileByName("Empty"));
                TileMap.SetCell((int)start.x - 1, (int)start.y + y, TileMap.TileSet.FindTileByName(y == 0 ? "TopRight" : (y == 6 ? "BottomRight" : "SideWall")));
                TileMap.SetCell((int)start.x + 1, (int)start.y + y, TileMap.TileSet.FindTileByName(y == 0 ? "TopLeft" : (y == 6 ? "BottomLeft" : "SideWall")));
            }
            if (connection.Type == "locked") {
                var doorPosition = sourceRect.Position
                    + new Vector2(Mathf.Floor(sourceRect.Size.x / 2), source.y < target.y ? sourceRect.Size.y - 1: 0);
                PlaceDoor((int)doorPosition.x, (int)doorPosition.y, connection);
            }
        }
        else if (source.y == target.y) {
            var start = (source.x < target.x ? sourceRect.Position : targetRect.Position)
                + new Vector2(sourceRect.Size.x - 1, Mathf.Floor(sourceRect.Size.y / 2));
            for (int x = 0; x < 7; x++) {
                TileMap.SetCell((int)start.x + x, (int)start.y, TileMap.TileSet.FindTileByName("Empty"));
                TileMap.SetCell((int)start.x + x, (int)start.y - 1, TileMap.TileSet.FindTileByName(x == 0 ? "BottomLeft" : (x == 6 ? "BottomRight" : "Top")));
                TileMap.SetCell((int)start.x + x, (int)start.y + 1, TileMap.TileSet.FindTileByName(x == 0 ? "TopLeft" : (x == 6 ? "TopRight" : "Top")));
            }
            if (connection.Type == "locked") {
                var doorPosition = sourceRect.Position
                    + new Vector2(source.x < target.x ? sourceRect.Size.x - 1 : 0, Mathf.Floor(sourceRect.Size.y / 2));
                PlaceDoor((int)doorPosition.x, (int)doorPosition.y, connection);
            }
        }
    }

    protected void PlaceGoal(Rect2 roomRect) {
        var goal = _goalPackedScene.Instance() as Node2D;
        _objectsContainer.AddChild(goal);
        goal.Position = (roomRect.Position + (roomRect.Size / 2.0f)).Floor() * 8.0f;
    }

    protected void PlaceDoor(int tileX, int tileY, Connection connection) {
        var door = _doorPackedScene.Instance() as Door;
        _objectsContainer.AddChild(door);
        door.Position = new Vector2(tileX * 8.0f, tileY * 8.0f);
        ConnectionDoorMap[connection] = door;
    }

    public void PlaceFloorSwitch(int gridX, int gridY, Connection connection) {
        if (!ConnectionDoorMap.ContainsKey(connection)) {
            return;
        }
        var roomRect = GetRoomRect(gridX, gridY);
        var floorSwitch = _floorSwitchPackedScene.Instance() as FloorSwitch;
        _objectsContainer.AddChild(floorSwitch);
        floorSwitch.Position = (roomRect.Position + new Vector2((GD.Randi() % (roomRect.Size.x - 2)) + 1, (GD.Randi() % (roomRect.Size.y - 2) + 1))).Ceil() * 8.0f;

        var door = ConnectionDoorMap[connection];
        floorSwitch.Connect("Pressed", this, "UnlockDoor", new Godot.Collections.Array(new object[]{ door }));
    }

    public void UnlockDoor(Door door) {
        door.Closed = false;
    }

}
