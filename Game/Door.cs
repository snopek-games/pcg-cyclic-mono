using Godot;
using System;

public class Door : StaticBody2D
{
    private Sprite ClosedSprite;
    private CollisionShape2D CollisionShape2D;

    private bool closed = true;

    [Export]
    public bool Closed {
        get {
            return closed;
        }
        set {
            closed = value;
            if (ClosedSprite != null) {
                ClosedSprite.Visible = closed;
            }
            if (CollisionShape2D != null) {
                CollisionShape2D.SetDeferred("disabled", !closed);
            }
        }
    }

    public override void _Ready() {
        ClosedSprite = GetNode<Sprite>("ClosedSprite");
        CollisionShape2D = GetNode<CollisionShape2D>("CollisionShape2D");
    }
}
