using Godot;
using System;

public class FloorSwitch : Area2D
{
    [Signal]
    delegate void Pressed();

    private Sprite OnSprite;
    private bool Switched = false;

    public override void _Ready() {
        OnSprite = GetNode<Sprite>("OnSprite");
    }

    public void _on_body_entered(PhysicsBody2D body) {
        if (!Switched) {
            Switched = true;
            OnSprite.Visible = true;
            EmitSignal(nameof(Pressed));
        }
    }

}
